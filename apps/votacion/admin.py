from django.contrib import admin
from apps.votacion.models import Mmvs, ReporteDiferencia, ReporteMenore
from django.db.models import F,Q



@admin.register(Mmvs)


class EscrutinioAdmin(admin.ModelAdmin):
    list_display = ("get_zona", "get_puesto","get_mesa", "get_cod_candidato","testigos","preconteo")
    ordering_fields = ("get_zona")
    list_filter = ( "codigo__puesto__cod_zona","codigo__puesto__puesto_name","codigo__no_mesa")
    search_fields = ("codigo__puesto__puesto_name",)
    list_per_page = 500


@admin.register(ReporteDiferencia)

class DiferenciaAdmin(admin.ModelAdmin):
    list_display = ("get_zona", "get_puesto","get_mesa", "get_cod_candidato","testigos","preconteo")
    ordering_fields = ("get_zona")
    list_filter = ( "codigo__puesto__cod_zona","codigo__puesto__puesto_name","codigo__no_mesa")
    search_fields = ("codigo__puesto__puesto_name",)
    list_per_page = 500


    def get_queryset(self, request):
        qs = super(DiferenciaAdmin, self).get_queryset(request)
        return qs.filter(votos__lt=F ("preconteo"))



@admin.register(ReporteMenore)

class MenoresAdmin(admin.ModelAdmin):
    list_display = ("get_zona", "get_puesto","get_mesa", "get_cod_candidato","testigos","preconteo")
    ordering_fields = ("get_zona")
    list_filter = ( "codigo__puesto__cod_zona","codigo__puesto__puesto_name","codigo__no_mesa")
    search_fields = ("codigo__puesto__puesto_name",)
    list_per_page = 500


    def get_queryset(self, request):
        qs = super(MenoresAdmin, self).get_queryset(request)
        return qs.filter(votos__gt=F ("preconteo"))