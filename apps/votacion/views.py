from django.views.generic import ListView, CreateView
from django.shortcuts import render
from apps.votacion.models import Votodetalle, Indicador, Divipol, Partido, Corporacion, Circunscripcion, Candidato, Mmvs
from apps.votacion.forms import IndicadorForm
from django.urls import reverse_lazy
from django.http import HttpResponse, QueryDict
from django.views import View, generic
from django.db.models import Q


iniciar = 0
departamento=16
municipio=1
corporacion=3
circunscripcion=2

print("numero de inicio "+str(iniciar))


class Uploadescrutinio(View):

    def get(self, request):
        return HttpResponse("ok")

class IndicadorList(generic.ListView):
    model = Mmvs
    queryset = Mmvs.objects.filter(codigo__cod_candidato__pk = "1196002")[:25]
    template_name = 'votacion/votacion_list.html'
    paginate_by = 32

class IndicadorCreate(CreateView):
    model = Indicador
    form_class = IndicadorForm
    template_name = 'votacion/votacion_form.html'
    success_url = reverse_lazy('votacion_listar')

def VerificacionList(request):
    return render(request, 'votacion/votacion_verificar.html', {
		'mmvs_all_count': Mmvs.objects.all().count(),
		'votodetalle_all_count': Votodetalle.objects.all().count(),
		'divipol_all_count': Divipol.objects.all().count(),
		'votodetalle_count_puestos': Votodetalle.objects.all().distinct('puesto__pk').count()
	})

# Para mostrar un listado de zonas y puestos de votación 
# asi poder seleccionar ya sea la zona completa o puesto individual
# con solo cuales ver reporte para la vistas
# ZonaDetail()
def ZonaList(request):
    return render(request, 'votacion/votacion_zona.html', {
		'zona_list': Divipol.objects.all().distinct('cod_zona'),
		'puesto_list': Divipol.objects.all().order_by('cod_puesto'),
	})

# Genera una tabla según zona[] y puesto[] seleccionados 
# este reporte muestra por puesto detalle MMVS y las cantidad
# de diferencia entra escrutinio1..2..3..4, preconteo y testigos
def ZonaDetail(request):
    puestos_list = Divipol.objects.filter(Q(idpuesto__in=request.GET.getlist('puesto')) | Q(cod_zona__in=list(QueryDict(request.GET['zona'])))).order_by('cod_zona','puesto_name')
    mesas_list = Votodetalle.objects.filter(puesto_id__in=puestos_list).select_related("mmvs", "cod_candidato")
    contexto = {'zonas': list(QueryDict(request.GET['zona'])),
                'puestos': request.GET.getlist('puesto'),
                'mesas_list': mesas_list,
                'puestos_list': puestos_list}
    return render(request, 'votacion/votacion_ver.html', contexto)


# Genera una tabla según zona[] y puesto[] seleccionados 
# este reporte muestra por puesto detalle MMVS y las cantidad
# de diferencia entra escrutinio1..2..3..4, preconteo y testigos
def ZonaDetailSimple(request):
    puestos_list = Divipol.objects.filter(Q(idpuesto__in=request.GET.getlist('puesto')) | Q(cod_zona__in=list(QueryDict(request.GET['zona'])))).order_by('cod_zona','cod_puesto')
    candidato = request.GET['candidato']
    opositor = request.GET['opositor']
    #q_escrutinional1 = ~Q(mmvs__preconteo__in != mmvs__escrutinional1)
    #q_escrutiniocd1 = Q(q_escrutinional1 != q_escrutiniocd1 & q_escrutiniocd1 != None)
    #q_escrutiniocd2 = Q(q_escrutinional2 != q_escrutiniocd2 & q_escrutiniocd2 != None)
    #q_escrutiniocd3 = Q(q_escrutinional3 != q_escrutiniocd3 & q_escrutiniocd3 != None)
    #q_escrutiniocd4 = Q(q_escrutinional4 != q_escrutiniocd4 & q_escrutiniocd4 != None)
    mesas_list = Votodetalle.objects.filter(puesto_id__in=puestos_list).select_related("mmvs", "cod_candidato").extra(where=["'mmvs__testigos' != 'None' AND ('mmvs__testigos' != 'mmvs__preconteo' AND 'mmvs__preconteo' != 'None')  OR  'mmvs__preconteo' != 'None' AND 'mmvs__preconteo' != 'mmvs__escrutinional1' AND 'mmvs__escrutinional01' != 'None' "],).order_by('no_mesa')
    contexto = {'zonas': list(QueryDict(request.GET['zona'])),
                'puestos': request.GET.getlist('puesto'),
                'mesas_list': mesas_list,
                'puestos_list': puestos_list,
                'candidato': candidato,
                'opositor': opositor}
    return render(request, 'votacion/votacion_ver_simple.html', contexto)
