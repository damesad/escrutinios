from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import login


urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuario/', include('apps.usuario.urls')),
    path('votacion/', include('apps.votacion.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
]
