import sys, os, django
from tqdm import tqdm
import threading
from concurrent.futures.thread import ThreadPoolExecutor
from time import sleep

def save_list(object_list):
    countdata = len(object_list)
    canthilos = 5
    moddata = countdata % canthilos
    divisordata = countdata - moddata
    fordata = divisordata // canthilos
    init = 1
    print("Carga en bd_base .. Preparate un Café se cargaran ", countdata , " registros.")
    result = ""
    with ThreadPoolExecutor() as executor:
        for i in range(1, canthilos):
            t = threading.Thread(target=saveRange, args=(init, fordata * i, object_list, result, ))
            init = fordata * i+1
            t.start()
        t = threading.Thread(target=saveRange, args=(divisordata, countdata, object_list, result, ))
    print(result)


def saveRange(init, finish, list_objects, result):
    desc = "Carga de " + str(init) + " a " + str(finish)
    for i in tqdm(range(init, finish), desc=desc):
        list_objects[i].save()
    result += "\n Carga de " + str(init) + " a " + str(finish) + " completa"