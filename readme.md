**DEPENDENCIAS**

Para correr el proyecto es necesario instalar 
las librerias, corriendo el comando rapido:

`pip install -r requirements.txt`

Si lo desea puede instalar cada paquete por individual.

`pip install psycopg2`
`pip install django-import-export`
`pip install django-suit`
`pip install django-mathfilters`
`pip install tqdm`

**MIGRACION**

El proyecto trabaja con POSTGRESS como motor de base de datos,
para realiza la migracion correr el comando.

`python manage.py migrate`

**SCRIPT DE IMPORTACION DE DATOS**

Para llenar los datos de la base de datos, es necesario correr
SCRIPT preparados segun el tipo de archivo para Escrutinio.

Se debe importar los datos en el siguiente orden:

`python start.py`
`python preconteo.py`
`python testigos.py`


**SUPER USUARIO**

Para administrar facilmente los Modelo de Datos la aplicación. es necesario crear un super usuario para acceder a `/admin` , ejecutando
el siguiente comando. 

`manage.py  createsuperuser`

**CORRER APLICACION**

Para ejetar la aplicacion en local ejecute el comando.

`manage.py runserver`

Opcionalmente se puede agregar el PUERTO.

`manage.py runserver 8040`