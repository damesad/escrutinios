import sys, os, django
from django.http import HttpResponse
import threading
from concurrent.futures.thread import ThreadPoolExecutor
from time import sleep
from happy_import import save_list
os.environ['DJANGO_SETTINGS_MODULE'] = 'elecciones.settings'
django.setup()
from apps.votacion.models import Votodetalle, Indicador, Divipol, Partido, Corporacion, Circunscripcion, Candidato, Mmvs, MmvGhost

iniciar = 1
departamento = 16
municipio = 1
corporacion = [3, 4]
circunscripcion = 2
votodetalle_list = list()

print("numero de inicio " + str(iniciar))


class UploadCircunscripcion():
    if iniciar == 1:
        file = open('basicos/circunscripcion.txt', mode='r')
        lines = file.readlines()
        file.close()
        for line in lines:
            columna = line.split(',')
            q = Circunscripcion(codigo=line[0:1], nombre=line[1:101])
            q.save()
        print("Carga de circunscripcion completa")

    def get(self, request):
        return HttpResponse("ok")


class UploadCorporacion():
    if iniciar == 1:
        file = open('basicos/corporacion.txt', mode='r')
        lines = file.readlines()
        file.close()
        for line in lines:
            columna = line.split(',')
            q = Corporacion(codigo=line[0:3], nombre=line[3:203])
            q.save()
        print("Carga de Corporacion completa")

    def get(self, request):
        return HttpResponse("ok")


class UploadIndicadoresCSV():
    if iniciar == 100:
        file = open('basicos/indicadores.csv', mode='r', encoding='utf-8-sig')
        lines = file.readlines()
        file.close()
        for line in lines:
            columna = line.split(',')
            q = Indicador(codigo=columna[0], nombre=columna[1], potencial=columna[2])
            q.save()

    def get(self, request):
        return HttpResponse("ok")


class UploadIndicadores():
    if iniciar == 1:
        file = open('basicos/indicadores.txt', mode='r', encoding='utf-8-sig')
        lines = file.readlines()
        file.close()
        for line in lines:
            columna = line.split(',')
            q = Indicador(codigo=line[0:1],
                          nombre=line[1:100],
                          potencial=line[101:105])
            q.save()
        print("Carga de Indicadores completa")

    def get(self, request):
        return HttpResponse("ok")


class UploadPartidos():
    if iniciar == 1:
        file = open('basicos/partidos.txt', mode='r')
        lines = file.readlines()
        file.close()
        for line in lines:
            columna = line.split(',')
            q = Partido(codigo=line[0:4],
                        nombre=line[4:204])
            q.save()
        q = Partido(codigo=0, nombre="REGISTRADURIA")
        q.save()
        print("Carga de Partidos completa")

    def get(self, request):
        return HttpResponse("ok")


class UploadDivipol():
    if iniciar == 1:
        file = open('basicos/divipol.txt', mode='r')
        lines = file.readlines()
        file.close()
        puestoid = 0
        for line in lines:
            comparedpto = int(line[0:2])
            comparemun = int(line[2:5])
            if comparedpto == departamento and comparemun == municipio:
                idindicador = Indicador.objects.get(codigo=line[91:92])
                puestoid = puestoid + 1
                q = Divipol(idpuesto=puestoid,
                            cod_dpto=line[0:2],
                            cod_mun=line[2:5],
                            cod_zona=line[5:7],
                            cod_puesto=line[7:9],
                            dpto_name=line[9:21],
                            mun_name=line[21:51],
                            puesto_name=line[51:91],
                            indicador=idindicador,
                            potencial_h=line[92:100],
                            potencial_m=line[100:108],
                            no_mesas=line[108:114],
                            cod_comuna=line[114:116],
                            comuna_name=line[116:146])
                q.save()
        print("Carga de Divipol completa")
        
    def get(self, request):
        return HttpResponse(self.line[108:114])


class UploadCandidatos():
    if iniciar == 1:
        file = open('basicos/candidatos.txt', mode='r')
        lines = file.readlines()
        file.close()
        max_concejo = 0
        count_consejo = False
        for line in lines:
            comparedpto = int(line[4:6])
            comparemun = int(line[6:9])
            comparecorp = int(line[0:3])
            if comparedpto == departamento and comparemun == municipio and comparecorp in  corporacion:
                columna = line.split(',')
                idcorporacion = Corporacion.objects.get(codigo=line[0:3])
                idcircunscripcion = Circunscripcion.objects.get(codigo=line[3:4])
                idpartido = Partido.objects.get(codigo=line[11:15])
                if int(line[15:18]) > 0:
                    codigo = line[11:18]
                    q = Candidato(codigoc=codigo, corporacion=idcorporacion,
                                  circunscripcion=idcircunscripcion,
                                  cod_dpto=line[4:6],
                                  cod_mun=line[6:9],
                                  cod_comuna=line[9:11],
                                  cod_partido=idpartido,
                                  preferente=line[18:19],
                                  nombre=line[19:69],
                                  apellido=line[69:119],
                                  cedula=line[119:134],
                                  genero=line[134:135],
                                  sorteo=line[135:136])
                    q.save()
                    if count_consejo is True:
                        max_concejo += 1
                else:
                    # PARA CORPORACION 3 ES TIPO DE VOTO ALCALDIA, 
                    # PARA CORPORACION 4 ES PARTIDO POLITICO CONCEJO 
                    q = Candidato(codigoc=codigo, corporacion=idcorporacion,
                                  circunscripcion=idcircunscripcion,
                                  cod_dpto=line[4:6],
                                  cod_mun=line[6:9],
                                  cod_comuna=line[9:11],
                                  cod_partido=idpartido,
                                  preferente=line[18:19],
                                  nombre=line[19:69],
                                  cedula=0,
                                  sorteo=line[135:136])
                    q.save()
                    count_consejo = corporacion == 4
        idcorporacion = Corporacion.objects.get(codigo=corporacion)
        idcircunscripcion = Circunscripcion.objects.get(codigo=circunscripcion)
        idpartido = Partido.objects.get(codigo=0)
        q = Candidato(codigoc="0000996",
                      corporacion=idcorporacion,
                      circunscripcion=idcircunscripcion,
                      cod_dpto=departamento,
                      cod_mun=municipio,
                      cod_comuna=0,
                      cod_partido=idpartido,
                      preferente=0,
                      nombre="VOTOS EN BLANCO",
                      cedula=0,
                      sorteo=0)
        q.save()
        q = Candidato(codigoc="0000997",
                      corporacion=idcorporacion,
                      circunscripcion=idcircunscripcion,
                      cod_dpto=departamento,
                      cod_mun=municipio,
                      cod_comuna=0,
                      cod_partido=idpartido,
                      preferente=0,
                      nombre="VOTOS NO MARCADOS",
                      cedula=0,
                      sorteo=0)
        q.save()
        q = Candidato(codigoc="0000998", corporacion=idcorporacion,
                      circunscripcion=idcircunscripcion,
                      cod_dpto=departamento,
                      cod_mun=municipio,
                      cod_comuna=0,
                      cod_partido=idpartido,
                      preferente=0,
                      nombre="VOTOS NULOS",
                      cedula=0,
                      sorteo=0)
        q.save()
        q = Candidato(codigoc="0000999",
                      corporacion=idcorporacion,
                      circunscripcion=idcircunscripcion,
                      cod_dpto=departamento,
                      cod_mun=municipio,
                      cod_comuna=0,
                      cod_partido=idpartido,
                      preferente=0,
                      nombre="VOTOS EN MESA",
                      cedula=0,
                      sorteo=0)
        q.save()
        print("Carga de Candidatos completa")

    def get(self, request):
        return HttpResponse("ok")


def pushVoto(i, item, cod_puesto, candidatos, cod_candidato996, cod_candidato997, cod_candidato998, cod_candidato999):
    cod_mesa = cod_puesto + "{:06d}".format(i)
    for candidato in candidatos:
        cod_mmv = cod_mesa + candidato.codigoc
        q = Votodetalle(puesto=item,
                        codigo=cod_mmv,
                        no_mesa=i,
                        cod_partido=candidato.cod_partido.codigo,
                        cod_candidato=candidato)
        votodetalle_list.append(q)
    cod_mmv = cod_mesa + "0000996"
    q = Votodetalle(puesto=item,
                    codigo=cod_mmv,
                    no_mesa=i,
                    cod_partido=0,
                    cod_candidato=cod_candidato996)
    votodetalle_list.append(q)
    cod_mmv = cod_mesa + "0000997"
    q = Votodetalle(puesto=item,
                    codigo=cod_mmv,
                    no_mesa=i,
                    cod_partido=0,
                    cod_candidato=cod_candidato997)
    votodetalle_list.append(q)
    cod_mmv = cod_mesa + "0000998"
    q = Votodetalle(puesto=item,
                    codigo=cod_mmv,
                    no_mesa=i,
                    cod_partido=0,
                    cod_candidato=cod_candidato998)
    votodetalle_list.append(q)
    cod_mmv = cod_mesa + "0000999"
    q = Votodetalle(puesto=item,
                    codigo=cod_mmv,
                    no_mesa=i,
                    cod_partido=0,
                    cod_candidato=cod_candidato999)
    votodetalle_list.append(q)


def saveVotos():
    save_list(votodetalle_list)


class UploadVoto():
    if iniciar == 1:
        items = Divipol.objects.all()
        candidatos = Candidato.objects.filter(corporacion=corporacion).select_related('cod_partido')
        print("Inicia Cargado de Votos - Esto puede demorar...")
        pre_cod_puesto = "{:02d}".format(departamento) + "{:03d}".format(municipio)
        cod_candidato996 = Candidato.objects.get(codigoc="0000996")
        cod_candidato997 = Candidato.objects.get(codigoc="0000997")
        cod_candidato998 = Candidato.objects.get(codigoc="0000998")
        cod_candidato999 = Candidato.objects.get(codigoc="0000999")
        with ThreadPoolExecutor() as executor:
            for item in items:
                cod_puesto = pre_cod_puesto + "{:02d}".format(item.cod_zona) + item.cod_puesto
                itmesas = int(item.no_mesas)
                for i in range(1, itmesas):
                    executor.submit(pushVoto,
                                    i,
                                    item,
                                    cod_puesto,
                                    candidatos,
                                    cod_candidato996,
                                    cod_candidato997,
                                    cod_candidato998,
                                    cod_candidato999)
                print('Puesto ', cod_puesto, ' Cargado Mesa Cant.', itmesas, )
        saveVotos()

    def get(self, request):
        return HttpResponse("ok")



# 004216